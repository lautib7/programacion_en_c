#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#define N 20 /*Cantidad de palabras que guarda el programa para el modo de 1 jugador*/


/*-------------------------------------------------------------------------------------*/
/*Declaración de estructuras*/


typedef struct dibujo /*Usada para las partes del cuerpo del ahorcado y a la horca*/
{	
int posicion; 
	char parte[8];
}Dibujo; /*Defino el tipo Dibujo*/


struct variables 
{
    int oportunidades;
    int correctas;
    int cantidad;
};
   
/*-------------------------------------------------------------------------------------*/
/*Prototipos de funciones*/


void pantalla_inicial (void); 
void inicio_juego ( char frase[], char guiones_o_letras[], struct variables *pdatos, char usadas[], Dibujo *horc, int modo);
int juego (struct variables *pdatos, int *p1, int *p2, Dibujo *horc, Dibujo *part, char usadas[], char guiones_o_letras[], char frase[], int gano);
void final_juego (int gano, char frase[], int *p1, int *p2); 
void borrar (void); 
void convierte_mayusculas (char *x);
int leer_numero(void);
int lee_modo (void);
void carga_frase (char frase[], int modo);
int crea_guiones (char frase[], char guiones[]);
int comprueba_letra_repetida (char usadas[], char letra);
void agrega_parte (int *parte, int *parte2, Dibujo *part, Dibujo *horc);
void imprime_pantalla(Dibujo *horc, char guiones_o_letras[], struct variables *pdatos, char usadas[]);
void imprime_horca (Dibujo *horc);
void imprime_guiones_o_letras (char guiones[]); 
void imprime_datos (struct variables *pdatos, char usadas[]);
char ingresa_letra (void);
void crea_usadas (char usadas[], int j, char letra);
int reemplaza_guion_por_letra (char guiones_o_letras[], char frase[], char letra, int bien, struct variables *pdatos);
int compara_original (char frase[], char guiones[], int gano); 
void mensajes_del_final (int gano, char frase[]);
void game_over (void); 


/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/


int main(void)
{
	Dibujo cuerpo[7]={{3,"|    O"},{4,"|   /"},{4,"|   /|"},{4,"|   /|\\"},
				{5,"|    |"},{6,"|   / "},{6,"|   / \\"}}; 


	Dibujo horca[8]={{1,"______"},{2,"|    |"},{3, "|    |"}, {4,"|"}, {5,"|"}, {6,"|"}, {7,"|"}, {8,"|"}};


	Dibujo *part, *horc;
	int gano=0, parte=3, parte2=0, modo;
	int *p1, *p2;
	char frase[100], guiones_o_letras[100], usadas[50]={0};
	struct variables datos={7, 0, 0}, *pdatos;


	/*Le damos valor a los punteros usados para imprimir el dibujo y los datos*/


	p1=&parte;
	p2=&parte2;
	part=cuerpo;
	horc=horca;
	pdatos=&datos;


	/*Inicio*/


	pantalla_inicial(); /*Solamente imprime*/ 
	modo=lee_modo(); /*El usuario selecciona el modo de juego */
   
	if(modo!=0) /*Si no se ha ingresado un 0 empieza el programa*/
	{
	   inicio_juego(frase, guiones_o_letras , pdatos, usadas, horc, modo); 
	   /*Crea los arreglos de la frase y los guiones, e imprime el dibujo*/
	   
	   gano=juego(pdatos, p1, p2, horc, part, usadas, guiones_o_letras, frase, gano);
	   /*La variable `gano' recibe un int que indica si el usuario ganó el juego*/
	   
	   final_juego(gano, frase, p1, p2); 
	   /*Imprime los mensajes de fin de juego y da la opción de volver a jugar llamando 	   otra vez a main()*/
	}
    
	return 0;
}/*Fin de main()*/


/*-------------------------------------------------------------------------------------*/


void inicio_juego ( char frase[], char guiones_o_letras [], struct variables *pdatos, 				  char usadas[], Dibujo *horc, int modo)
{
	carga_frase(frase, modo); /*El arreglo frase recibe una palabra o frase según el modo elegido*/
	/*Creo guiones_o_letras*/
	pdatos->cantidad = crea_guiones(frase, guiones_o_letras);/*Cantidad de letras sin contar espacios*/
	
	imprime_pantalla(horc, guiones_o_letras, pdatos, usadas); /*Imprime el dibujo, guiones y datos*/
}


/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


int juego (struct variables *pdatos, int *p1, int *p2, Dibujo *horc, Dibujo *part, char usadas[], char guiones_o_letras[], char frase[], int gano)
{
	char letra;
	int j=0, bien=0;
	do{
		bien=0; /*Se usa más adelante*/
		
		letra=ingresa_letra();/*Se ingresa una letra que se guarda en el char 'letra'*/


		if(letra=='*') /*Si ingresa un * hay que terminar el programa*/
			break; /*Al ingresar un * termina el bucle del do while*/
		
		/*Compruebo si la letra ingresada es correcta*/


		if(!comprueba_letra_repetida(usadas, letra))/*Si no estaba repetida la letra, la función devuelve un 0*/
		{
			crea_usadas (usadas, j, letra); /*Agrega la letra al arreglo de las repetidas*/
			bien=reemplaza_guion_por_letra (guiones_o_letras, frase, letra, bien, pdatos);
			j++; /*j controla al arreglo 'usadas'*/
			
			if(bien == 0) /*Si bien vale 0, significa que se equivocó*/
			{
				pdatos->oportunidades-=1; /*Se pierde una oportunidad*/
				agrega_parte(p1, p2, part, horc);/*Esto reemplaza una parte de la horca por una parte del cuerpo*/
			}
			borrar(); /*Borra la pantalla*/
			
			imprime_pantalla(horc, guiones_o_letras, pdatos, usadas);
		}
		else /*Si la letra estaba repetida*/
		{
			printf(" Ya ha introducido la letra '%c' en esta partida.\n", letra);
		}
		gano=compara_original (frase, guiones_o_letras, gano); /*Si está completa, 'gano' toma el valor 1*/
		
	} while(pdatos->oportunidades!=0 && gano!=1);


	/*Se repite mientras la palabra no esté completa o aún queden oportunidades*/


	return gano;
}


/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/


void final_juego (int gano, char frase[], int *p1, int *p2)
{int repetir;
	
	mensajes_del_final(gano, frase);/*Mensajes del final del juego*/
	
	printf(" ¿Desea jugar de nuevo? \n Ingrese '1' para volver a jugar o cualquier número para terminar: ");
	repetir=leer_numero(); 
	borrar(); /*Borra la pantalla*/
	
	if(repetir==1) /*Si ingresa un 1, se vuelve a llamar a la funcion main y comienza 	todo de nuevo*/
	{
		/*Reiniciamos las variables correspondientes a imprimir el dibujo*/
		*p1=3;
		*p2=0;
		main();
	}
	else
	   game_over(); /*Imprime 'Game Over' y termina el programa*/
}


/*-------------------------------------------------------------------------------------*/


void pantalla_inicial (void)
{
	printf(" x=======<>=======x\n\n");
	printf(" JUEGO DEL AHORCADO\n\n");
	printf(" x=======<>=======x\n\n\n");
	
	printf(" Modos de juego: \n\n");
	printf(" Opción 1 - Adivinar una palabra dada por la computadora\n");
	printf(" Opción 2 - Un jugador ingresa una palabra y otro adivina\n\n");
}


/*-------------------------------------------------------------------------------------*/


void borrar (void) /*Borra la pantalla*/
{
	if (system( "cls" )) /*Borra la pantalla en Windows*/
		system( "clear" ); /*Borra la pantalla en Linux*/
}


/*-------------------------------------------------------------------------------------*/


void convierte_mayusculas (char *x)/*Función que convierte las mayúsculas en minúsculas*/
{
	if (*x>='A' && *x<='Z')
		*x+=' ';
}


/*-------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------*/


int leer_numero(void)/*Valida si ingresa una letra en lugar de un número*/
{
int numero, error;
	error = 0;
	while (!error) /*Se repite hasta ingresar un número*/
	{
		error = scanf("%d",&numero); /*Si se ingresa un número, error vale 1, si se ingresa una letra, vale 0*/
		if (!error) /*Si se ingresó una letra, se pide un número*/
		{
			getchar();
			printf(" Debe ingresar un número: ");
		}	
	}
	return numero; /*Devuelve el numero ingresado luego de determinar que no es una 	letra*/
}


/*-------------------------------------------------------------------------------------*/


int lee_modo (void) /*Lee el modo elegido por el usuario*/
{
	int modo;
	printf(" Ingresar '1' o '2' para elegir el modo de juego (o ingresar '0' para salir): ");
	switch(leer_numero()) /*Utiliza leer_numero para que solo se pueda ingresar un número*/
	{
	case 0:
        modo=0;
		borrar(); /*Función que borra la pantalla*/
		game_over();/*Función que termina el programa*/
		break;
	case 1:
		modo = 1;
		break;
	case 2:
		modo = 2;
		break;
	default:	/*En caso de que el número ingresado no sea 0, 1 ó 2*/
		modo=lee_modo(); /*Función recursiva, pide un número hasta que este coincida con alguno de los casos anteriores*/
	}
	getchar();
	return modo;
}


/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


void carga_frase (char frase[], int modo) /*Según el modo elegido, carga la palabra o frase en el arreglo 'frase'*/
{
	/*Estas son todas las palabras que pueden ser elegidas aleatoriamente en el modo 1*/


	char palabras[N][9]={"anteojo","avioneta","zapallo","bermejo",
			"espejo","anzuelo","trapecio","cuchara",
			"insecto","agujero","carnada","mortaja",
			"añejo","probeta","catarsis","harapos",			
			"arcaico","estrago","cortejo","capataz"}; 
	int i, o;


	char *mayus;


	borrar(); /*Borra la pantalla*/


	if (modo==1) /*Opción 1: 1 jugador*/
	{
		srand(time(NULL));  /*Genera la semilla para rand() en base al tiempo*/
		o=rand()%N; 	/*Crea un número aleatorio entre 0 y N-1*/
		
		strcpy(frase, palabras[o]); /*El arreglo 'frase' toma el valor de la palabra 		con el número generado*/
	}
	if (modo==2) /*Opción 2: 2 jugadores*/
	{	
		printf(" Introduzca la palabra o frase a adivinar: \n\n");
		fgets(frase, 50, stdin);/*fgets cumple la función del scanf*/
		if (frase[strlen(frase)-1] == '\n')/*fgets agrega un \n al final*/
			frase[strlen(frase)-1] = '\0'; /*Reemplazo el \n con un final de 										cadena*/
		for(i=0 ; frase[i]!=0 ; i++) 
		{
			mayus=&frase[i]; 
			convierte_mayusculas(mayus);/*Si la frase tiene alguna mayúscula, la 				convertimos en minúscula*/
		}
	}
	borrar(); /*Borra la pantalla*/
}
/*-------------------------------------------------------------------------------------*/


int crea_guiones (char frase[], char guiones[]) /*Crea el arreglo guiones_o_letras*/
{
	int i, cantidad, longitud=0, espacios=0;
	
	for(i=0 ; i<strlen(frase) ; i++)   /*strlen devuelve el largo de la cadena*/
	{
		if(frase[i] == ' ') 
		{
			guiones[i] = ' ';
			longitud++;
			espacios++;
		}
		else 
		{
			guiones[i] = '_'; 
			longitud++;
		}
	}
	guiones[longitud] = '\0'; /*Agrego un final de cadena cuando termina la palabra*/


	cantidad = strlen(guiones)-espacios;/* Cantidad de letras sin contar espacios*/
return cantidad;
}


/*-------------------------------------------------------------------------------------*/


int comprueba_letra_repetida (char usadas[], char letra) /*Comprueba si la letra ingresada se halla en el arreglo 'usadas'*/
{
	int i, a=0;
	for(i=0;i<strlen(usadas);i++) 
	{
		if(usadas[i] == letra) 
		{
			a=1; 
			break;
		}
		else
		    a=0;
	}
	return a; /*Si la letra estaba repetida, a=1; sino, a=0*/
}


/*-------------------------------------------------------------------------------------*/


void agrega_parte (int *parte, int *parte2, Dibujo *part, Dibujo *horc)/*Reemplaza una parte de la horca con una parte del cuerpo del ahorcado*/
{			
	strcpy(horc[*parte].parte, part[*parte2].parte); /*strcpy reemplaza una parte del arreglo 'dibujo' por una parte del cuerpo*/
	/*Aumentan las variables correspondientes a las partes a reemplazar*/
	if(*parte2==0 || *parte2==3 || *parte2==4) 
		*parte=*parte+1;
	*parte2=*parte2+1;
}


/*-------------------------------------------------------------------------------------*/


void imprime_pantalla(Dibujo *horc, char guiones_o_letras[], struct variables *pdatos, char usadas[])
{
	imprime_horca(horc);/*Imprime horca*/
	
	imprime_guiones_o_letras (guiones_o_letras);/*Imprime las letras ingresadas o 	guiones*/
	
	imprime_datos(pdatos, usadas); /*Imprime los datos debajo del dibujo*/
}


/*-------------------------------------------------------------------------------------*/


void imprime_horca (Dibujo *horc) /*Imprime la horca y el cuerpo*/
{
	int i;
	for(i=0 ; i<8 ; i++)
		printf("  %s\n", horc[i].parte);
	printf("\n\n\n");
}
/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


void imprime_guiones_o_letras (char guiones[]) /*Imprime los guiones que representan a la palabra*/
{
	int i;
	for(i=0;i<strlen(guiones);i++) 
	{
		printf(" %c ",guiones[i]); /*Imprime los guiones y las letras correctas*/
	}
}


/*-------------------------------------------------------------------------------------*/


void imprime_datos (struct variables *pdatos, char usadas[]) /*Imprime los datos debajo del dibujo*/
{
	int i;
	printf("\n\n\n");
	printf(" Letras restantes: %d \n", pdatos->cantidad - pdatos->correctas); /*Imprime 	la cantidad de letras que hay en la palabra*/
	printf(" Letras ingresadas: ");
	for(i=0 ; i<strlen(usadas) ; i++) /*Este for imprime todas las letras que ya se 								usaron hasta ahora*/
	{
		printf(" %c -",usadas[i]);
	}
	printf("\n");
	printf(" Letras acertadas: %d", pdatos->correctas); /*Imprime la cantidad de letras 	correctas*/
	printf("\n");
	printf(" Oportunidades restantes: %d", pdatos->oportunidades); /*Imprime las 	oportunidades que quedan*/
	printf("\n");
}


/*-------------------------------------------------------------------------------------*/


char ingresa_letra (void) /*Pide el ingreso de la letra a adivinar*/
{
	char *mayus, letra;
	printf("\n Ingrese una letra (o ingrese * para salir): ");		
	scanf(" %c", &letra); /*Lee la letra ingresada*/
	mayus=&letra;
	convierte_mayusculas(mayus); /*Convierto la letra en minúscula si el usuario ingresa 	una mayúscula*/


	return letra;
}


/*-------------------------------------------------------------------------------------*/


void crea_usadas (char usadas[], int j, char letra) /*Guarda la letra en un arreglo que tiene todas las letras ya usadas*/
{
	usadas[j]=letra; 
}


/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


int reemplaza_guion_por_letra (char guiones_o_letras[], char frase[], char letra, int bien, struct variables *pdatos)
{
	int i;


	for(i=0 ; i<strlen(frase) ; i++) /*Este for reemplaza un _ del arreglo 	guiones_o_letras por la letra correcta */
	{
		if(frase[i] == letra)
		{
			guiones_o_letras[i] = letra; /*Reemplaza un guión por la letra correcta 			ingresada*/
			pdatos->correctas+=1; /*Aumenta la cantidad de letras correctas 					ingresadas*/
			bien=1; 
		}
	}
	return bien; /*Si se encontró una letra correcta, 'bien' vale 1; sino, vale 0*/
}


/*-------------------------------------------------------------------------------------*/


int compara_original (char frase[], char guiones[], int gano) /*Compara la frase original con la que ve el usuario*/
{
	if(strcmp(frase,guiones) == 0) /*strcmp compara si lo que está en el arreglo 	guiones_o_letras es igual a la frase original*/
	{
		gano = 1;
		/*Termina el do while*/
	}
	return gano; /*Si las palabras son iguales, 'gano' vale 1; sino, vale 0*/
}


/*-------------------------------------------------------------------------------------*/


void mensajes_del_final (int gano, char frase[]) /*Mensajes del final del juego*/
{
	if(gano) 
	{
		/*Mensaje si ganó el juego*/
		printf("\n\n");
		printf(" ***************************\n");
		printf(" Felicitaciones, has ganado.\n");
		printf(" ***************************\n\n");
		printf("\n");
	}
	else 
	{
		/*Mensaje si perdió el juego*/
		printf("\n\n");
		printf(" Has perdido.");
		printf("\n\n");
		printf(" La palabra era '%s'.", frase);/*Imprime la frase o palabra correcta*/
		printf("\n");
	}
}


/*-------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------*/


void game_over (void)
{
printf("\n");
	printf("	     *********    *****    ***     ***  ********\n");
	printf("	    **********   *******   ****   ****  ********\n");
	printf("	   ***          ***   ***  ***********  ***     \n");
	printf("	   ***   *****  *********  ***********  ******  \n");
	printf("	   ***   *****  *********  *** *** ***  ******  \n");
	printf("	   ***     ***  ***   ***  ***     ***  ***     \n");
	printf("	    **********  ***   ***  ***     ***  ********\n");
	printf("	     ********   ***   ***  ***     ***  ********\n\n");


	printf("	     *******   ***    ***  ********  *********\n");
	printf("	    *********  ***    ***  ********  **********\n");
	printf("	    ***   ***  ***    ***  ***       ***    ***\n");
	printf("	    ***   ***  ***    ***  ******    *********\n");
	printf("	    ***   ***  ***    ***  ******    ********\n");
	printf("	    ***   ***   ***  ***   ***       ***   ***\n");
	printf("	    *********    ******    ********  ***    ***\n");
	printf("	     *******      ****     ********  ***     ***\n\n");


	exit(0); /*Finaliza el programa*/
}


/*Fin del programa*/


/*-------------------------------------------------------------------------------------*/

