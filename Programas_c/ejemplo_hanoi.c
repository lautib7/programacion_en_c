#include <stdio.h>
#include <string.h>


void Fila(int n)
{
    char fil[10] = "_";
    if (n<=0)
    {
        printf(" ");
    }
    else
    {
        for(int i=1; i<n; i++)
        {
            strcat(fil, "_");
        }
        printf(fil);
    }
}

void Hanoi(int a, int n, int torre_a, int torre_b, int torre_c)
{
    int j = 0;
    int k = 0;
    if(n == 1)
    {   
        printf("De %i a %i\n", torre_a, torre_c);
        if(a%2 != 0)
        {
            for(int i = 0; i < a; i++)
            {
                Fila(j);
                printf("\t");
                Fila(0);
                printf("\t");
                Fila(k-(a-2));
                printf("\n");
                j++;
                if(k==0)
                {
                    j=j+1;
                }
                k++;
                
            }
        }
    }

    else
    {
    Hanoi(a, (n-1), torre_a, torre_c, torre_b);
    printf("De %i a %i\n", torre_a, torre_c);
    Hanoi(a, (n-1), torre_b, torre_a, torre_c);
    }
}

int main()
{
Hanoi(3,3,1,2,3);
}