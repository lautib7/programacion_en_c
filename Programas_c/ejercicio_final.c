#include "stdio.h"
#include "stdlib.h"
#include "conio.h"
#include "string.h"


struct estruct_alumnos
{
    char nombre[20];
    char dni[20];
    int edad;
};

int main()
    {
        struct estruct_alumnos alumnos[3];


        for(int i = 0; i < 3; i++)
        {
            printf("Ingrese el nombre de el alumno %i: ", i+1);
            gets(alumnos[i].nombre);
        
            printf("Ingrese el dni(sin espacios) de el alumno %i: ", i+1);
            gets(alumnos[i].dni);

            printf("Ingrese la edad de el alumno %i: ", i+1);
            scanf("%i", &alumnos[i].edad);

            while(getchar() != '\n');
        }

        int j = 0;
        char salir[20];

        while (strcmp("salir", salir) != 0)
        {
            j = 0;
            printf("Ingrese el dni que desea buscar: ");
            gets(salir);
            while(j < 3)
            {
                if(strcmp(salir, alumnos[j].dni) == 0)
                {
                    printf("El nombre del alumno es: %s\n", alumnos[j].nombre);
                    printf("El dni del alumno es: %s\n", alumnos[j].dni);
                    printf("La edad del alumno es: %i\n", alumnos[j].edad);
                    j = 3;
                }
                else if(strcmp(salir, alumnos[j].dni) != 0 && j == 2)
                {
                    printf("El dni ingresado es incorrecto.\n");
                    j = 3;
                }
                else if(strcmp(salir, "salir") == 0)
                {
                    j = 3;
                }
                else
                {
                    j++;
                }
            }
        }
    
    }